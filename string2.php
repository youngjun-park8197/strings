<!-- You can see how it's different between ' ' and " " 
if you want to add character right after other characters, then
use '.'
-->

<?php
    $a = array('hello', 'world');
    echo "생활코딩의 공식인사는 {$a[0]} {$a[1]}입니다";
    echo '생활코딩의 공식인사는 '.$a[0].' '.$a[1].'입니다';
?>



<?php
  $a = "생활";
  $b = "코딩";
  echo $a.$b;
?>